# Fatal Error Screen

A **Vue** plug-in that uses a **splash screen** to inform users of **non-recovering critical errors**.

## Features

* **Fully** customizable splash screen
* Store all **error definitions** inside a single **dictionary**
* Can be invoked anywhere using an exposed **Vue global method**

## Install

    <script type="text/javascript" src="/path/to/v-plugIn_fatalError_screen.js"></script>

## Basic Usage

    Vue.use(fatalError_screen, {
        dictionary: {
            key: {
                name: [string],
                reason: [string],
                description: [string]
            }
        }
    });

Fill the **dictionary** option with all the needed **error definitions**.

    Vue.throw_fatalError(dictionary_key);

The splash screen can be summoned by using a **global Vue method**.

## User Manual

[Fatal Error Screen User Manual](https://bitbucket.org/thebitbros/fatal-error-screen/src/master/docs/manual.md)

## Reporting Bugs

We are **not tracking** bug issues for now, but we'll do so later on.

## License

This software is distributed under the term of [GPLv3](https://bitbucket.org/thebitbros/fatal-error-screen/src/master/docs/license.txt).