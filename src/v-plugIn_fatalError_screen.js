"use-strict";

const fatalError_screen = {
    install (Vue, options) {
	Vue.throw_fatalError = function (dictionary_key) {

	    const dictionary = options.dictionary,
		  create_errorScreen = () => {
		      
		      const screen_section = document.createElement('section'),
			    error = dictionary.hasOwnProperty(dictionary_key) ? dictionary[dictionary_key] : (
				console.warn('No dictionary provided -- fallback error screen data used.'),
				{
				    name: 'FATAL ERROR',
				    reason: 'Unknown reason',
				    description: 'Please try again later. Thank you.'
				}
			    );
		      
		      screen_section.className = 'fatalError';
		      screen_section.style.cssText = 'left:0;position:absolute;top:0;z-index:10;';
		      screen_section.innerHTML = `<div><header><h1>${error.name}</h1><h3>${error.reason}</h3></header><p>${error.description}</p></div>`;
		      return screen_section;
		  };

	    console.trace();
	    document.body.appendChild(create_errorScreen());
	    return;
	};
    }
};
