# Fatal Error Screen - User Manual

The _Fatal Error Screen_ **Vue plug-in** displays a **splash screen** to inform users that a **critical, non-recovering error** happened, providing it's **name**, **reason** and a brief **description**. A common use case is when dealing with services or applications wholly dependent on the **availability** of **incoming data** or some other **external resource**.

## Installation

    <script type="text/javascript" src="/path/to/v-plugIn_fatalError_screen.js"></script>

Regarding browser compatibility, it's fine as long as it supports **basic ES6 syntax**.

## Configuration

    Vue.use(fatalError_screen, {
        dictionary: {
            key: {
                name: [string],
                reason: [string],
                description: [string]
            }
        }
    });

The plug-in only accepts a `dictionary` option, which is an object with all error definitions therein. Each definition must be an object with `name`, `reason` and `description` properties, whose values must be of _string_ type.

## Usage

    Vue.throw_fatalError(dictionary_key);

When deployed, a **global Vue** method will be exposed, allowing ubiquitous invocation using a **definition entry** (object key) as it's sole argument (`dictionary_key`).

If a **non-existent** dictionary key is used, or no `dictionary` option was provided in the first place, a **fallback** error definition will be used **automatically**.

The result of calling this method will be:

* A **stack trace** will be printed in the browser's console by using `console.trace()`.

* **Appending** of the splash screen to the **document body** with the chosen (or fallback) **definition data**.

## Styling

It is up to the developer to choose and apply all of the presentational aspects of the splash screen. As always, our recommendation is to employ mere **vanilla CSS**.

## Reporting Bugs

**This plug-in is provided as-is**, without any kind of support for now. Nevertheless, development continuity is guaranteed.